# animal-api-in-go

This is a basic run at an animal listing API.  I have it listing the first 50 animals from a database of over 900,000.  Some of the values are null, so that had to be accounted for.

This example uses MySql and GoLang running on Apache.

This is visible live at http://go.theanimaldatabase.com:8080/getAnimals