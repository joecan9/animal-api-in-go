package main

import (
	"fmt"
	"log"
	"reflect"
	"net/http"
	"encoding/json"
	"database/sql"

	"github.com/gorilla/mux"
)

import _ "github.com/go-sql-driver/mysql"

//Home link  /
func homeLink(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Welcome home!")
}

//Get Animals   /getAnimals
func getAnimals(w http.ResponseWriter, r *http.Request) {
    db, err := sql.Open("mysql", "joecan9_whatzoo:Tigger87!@tcp(127.0.0.1:3306)/joecan9_whatzoo")
    var animals []Animal;
	
	results, err := db.Query("SELECT a.id as 'Id', k.name as 'Kingdom', p.name as 'Phylum', c.name as 'Class', o.name as 'Order', f.name as 'Family', sf.name as 'Subfamily', g.name as 'Genus', sg.name as 'Subgenus', s.name as 'SpecificEpithet', ss.name as 'Subspecies', cn.name as 'CommonName' FROM joecan9_whatzoo.Animal a inner join Taxonomy k on a.kingdom_id = k.id inner join Taxonomy p on a.phylum_id = p.id inner join Taxonomy c on a.class_id = c.id inner join Taxonomy o on a.order_id = o.id inner join Taxonomy f on a.family_id = f.id inner join Taxonomy g on a.genus_id = g.id inner join Taxonomy s on a.species_id = s.id left join Taxonomy sf on a.subfamily_id = sf.id left join Taxonomy sg on a.subgenus_id = sg.id left join Taxonomy ss on a.subspecies_id = ss.id left join Taxonomy cn on a.commonName = cn.id limit 50")
	
    for results.Next() {
    	var animal Animal
    	
    	err = results.Scan(&animal.ID, &animal.Kingdom, &animal.Phylum, &animal.Class, &animal.Order, &animal.Family, &animal.Subfamily, &animal.Genus, &animal.Subgenus, &animal.SpecificEpithet, &animal.Subspecies, &animal.CommonName)
        if err != nil {
            panic(err.Error()) // proper error handling instead of panic in your app
        }

		animals = append(animals,animal)
    }
    
	w.WriteHeader(http.StatusCreated)
    json.NewEncoder(w).Encode(animals)
}

//Main
func main() {
	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/", homeLink)
	router.HandleFunc("/getAnimals", getAnimals)
	log.Fatal(http.ListenAndServe(":8080", router))
}

//types and structs
type NullString sql.NullString
type String string

//Animal Structure
type Animal struct {
    ID   int    `json:"id"`
    Kingdom string `json:"Kingdom"`
    Phylum string `json:"Phylum"`
    Class string `json:"Class"`
    Order string `json:"Order"`
    Family string `json:"Family"`
    Subfamily NullString `json:"Subfamily"`
    Genus string `json:"Genus"`
    Subgenus NullString `json:"Subgenus"`
    SpecificEpithet string `json:"SpecificEpithet"`
    Subspecies NullString `json:"Subspecies"`
    CommonName NullString `json:"CommonName"`
}

// Scan implements the Scanner interface for NullString
func (ns *NullString) Scan(value interface{}) error {
	var s sql.NullString
	if err := s.Scan(value); err != nil {
		return err
	}

	// if nil then make Valid false
	if reflect.TypeOf(value) == nil {
		*ns = NullString{s.String, false}
	} else {
		*ns = NullString{s.String, true}
	}

	return nil
}

// MarshalJSON for NullString
func (ns *NullString) MarshalJSON() ([]byte, error) {
	if !ns.Valid {
		return []byte("null"), nil
	}
	return json.Marshal(ns.String)
}
